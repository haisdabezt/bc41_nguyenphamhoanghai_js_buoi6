// bài 1
const minNd = () => {
  var n = 1;
  for (var sum = 0; sum < 10000; n++) {
    sum += n;
    document.getElementById(
      "minnd"
    ).innerHTML = `👉 Số nguyên dương nhỏ nhất : ${n}`;
  }
};

// bài 2
const tinhTong = () => {
  var x = document.getElementById("nhapx").value * 1;
  var n = document.getElementById("nhapn").value * 1;
  var sum = 0;
  for (var i = 1; i <= n; i++) {
    sum += Math.pow(x, i);
  }
  document.getElementById("tinhtong").innerHTML = `👉 Tổng : ${sum}`;
};

// bài 3
const tinhGiaiThua = () => {
  var n = document.getElementById("giaithua").value * 1;
  var n1 = 1;
  for (var i = 1; i <= n; i++) {
    n1 *= i;
  }
  document.getElementById("tinhgiaithua").innerHTML = `👉 Giai thừa : ${n1} `;
};

// bài 4
const taoTheDiv = () => {
  var div = document.querySelectorAll(".div");
  for (var i = 0; i < div.length; i++) {
    if (i % 2 == 0) {
      div[i].style.background = "blue";
    } else {
      div[i].style.background = "red";
    }
  }
  document.getElementById("taothediv");
};
